# 闭包
1.是一个被（外部）函数返回的函数。

2.是个被返回的函数，使用了外部的函数的变量或是参数，使得用到得变量或者参数不能被释放.

```js
function abc(arr) {
    let sum=function () {
        return arr.reduce(function (x,y) {
            return x+y;
        });
    }
    return sum;
}

let f=abc([1,2,3,4,5]);
console.log(f());
```

```js
用函数求平方和立方
function pow(n) {
    return function (x) {
        return Math.pow(x,n);//x代表底数，n代表指数
    }
}
let pow2=pow(2);//这个是平方
    let sum=function () {
        return arr.reduce(function (x,y) {
            return x+y;
        });
    }
    return sum;

let f=abc([1,2,3,4,5]);
console.log(f());
```

```js
用函数求平方和立方
function pow(n) {
    return function (x) {
        return Math.pow(x,n);//x代表底数，n代表指数
    }
}
let pow2=pow(2);//这个是平方
let pow3=pow(3);//这个是立方

console.log(pow2(5));
console.log(pow3(5));
```